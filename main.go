package main

import (
	"image"
	"image/color"
	"math/rand"
	"math"
	"hash/crc32"
	"net/http"
	"log"
	"bytes"
	"strconv"
	"net/url"
	"strings"
	"time"
	"image/png"
	"image/jpeg"
)

func main() {
	width := 600
	height := 300
	seed := ""
	columns := 3

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		u, err := url.Parse(r.RequestURI)
		if err != nil {
			panic(err)
		}
		trimmedPath := strings.Trim(u.Path, "/")
		pathArray := strings.Split(trimmedPath, "/")
		if len(pathArray) == 3 {
			width, _ = strconv.Atoi(pathArray[0])
			height, _ = strconv.Atoi(pathArray[1])
			seed = pathArray[2]
		} else if len(pathArray) == 2 {
			width, _ = strconv.Atoi(pathArray[0])
			height, _ = strconv.Atoi(pathArray[0])
			seed = pathArray[1]
		} else {
			seed = trimmedPath
		}
		generatedImg := generateImage(width, height, columns, seed)
		wImg := generatedImg.SubImage(image.Rect(0, 0, width, height))
		fileEnding := strings.Split(trimmedPath, ".")
		format := strings.ToLower(fileEnding[len(fileEnding)-1])
		writeImage(w, &wImg, format)
	})
	log.Fatal(http.ListenAndServe(":80", nil))

}

func writeImage(w http.ResponseWriter, img *image.Image, format string) {

	buffer := new(bytes.Buffer)
	if format == "jpg" || format == "jpeg" {
		if err := jpeg.Encode(buffer, *img, nil); err != nil {
			log.Println("unable to encode image.")
		}
		w.Header().Set("Content-Type", "image/jpg")
	} else {
		if err := png.Encode(buffer, *img); err != nil {
			log.Println("unable to encode image.")
		}
		w.Header().Set("Content-Type", "image/png")
	}
	w.Header().Set("Content-Length", strconv.Itoa(len(buffer.Bytes())))
	if _, err := w.Write(buffer.Bytes()); err != nil {
		log.Println("unable to write image.")
	}
}

func generateImage(width int, height int, columns int, seed string) *image.RGBA {

	if seed == "" {
		t := time.Now()
		seed = t.Format("20060102150405")
	}
	if width < 1 {
		width = 1
	}
	if height < 1 {
		height = 1
	}
	for width % columns != 0 {
		columns = columns + 1
		if columns > 100 {
			columns = 1
		}
	}
	generatedImg := image.NewRGBA(image.Rectangle{image.Point{0, 0}, image.Point{width, height}})
	modulobreak := int(math.Floor(float64(width / columns)))
	colors := make([]color.RGBA, columns)
	s1 := rand.NewSource(int64(crc32.ChecksumIEEE([]byte(seed))))
	r1 := rand.New(s1)

	for col := 0; col < columns; col++ {
		colors[col] = color.RGBA{uint8(r1.Intn(255)), uint8(r1.Intn(255)), uint8(r1.Intn(255)), 255}
	}

	i := 0
	c := colors[i]

	for x := 0; x < width; x++ {
		for y := 0; y < height; y++ {
			generatedImg.Set(x, y, c)
		}
		if x % modulobreak == 0 {
			i++;
			if i == columns {
				i = 0
			}
			c = colors[i]
		}
	}
	return generatedImg
}